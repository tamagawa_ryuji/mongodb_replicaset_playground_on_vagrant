# What is this ?

With these files, you can build a playground for trying MongoDB's Replica Set.

# Prerequisites

* VirtualBox installed
* Vagrant installed
* Internet connection

# How to use

cd to the MongoReplicaSet directory and '`vagrant up`'.
Then one of the box files listed on Vagrantbox.es and RPM packages for MongoDB will be downlaoded.   
When vagrant up is done, you have this playground:

* three hosts, mongo1, mongo2, mongo3 with 192.168.111,101, 192.168.111,102, 192.168.111,103, respectively.
* MongoDB's RPM packages, MongoDB-10gen and MongoDB-10gen-servers, are installed in the hosts.
* MongoDB's services are up and running in the hosts wihh a configuration for ReplicaSet ('replSet = rs0' in /etc/mongod.conf), but the Replica Set is not initialized yet.
* The hosts are within a hostonly-network of VirtualBox.

# Try!

You can initialize the ReplicaSet by:

1. SSH into mongo1 (`vagrant ssh mongo1`)
2. start MongoDB shell (`mongo`)
3. Initialize the ReplicaSet (`rs.initiate()`)
4. Add members (`rs.add("mongo2")` then `rs.add("mongo3")`)

**Have fun!**
